# sniper Platform images

This project provides container images for the Steam Runtime 3 'sniper'
Platform. These images can be used to test games and other software
that has been compiled for the sniper environment.

For general information about sniper, please see the README in the
`steamrt/sniper` branch of the `steamrt` package:
<https://gitlab.steamos.cloud/steamrt/steamrt/-/blob/steamrt/sniper/README.md>.

## Developing software that runs in sniper

Please see steamrt/sniper/sdk> for details.

## Testing software that runs in sniper

The primary way to test software that runs in sniper is to install it
via Steam.
For games that have not yet been published on Steam, sniper can be downloaded
with a command like `steam steam://install/1628350`,
and the game can be added to Steam as a shortcut, either by using Steam's
[non-Steam game][] feature or by using the [devkit tool][].

It is also possible to set up the Steam container runtime
[without using Steam](#slr-without-steam).

On continuous integration systems where it is not straightforward to install
Steam or the container runtime, an alternative to this is to use the
Platform [OCI images](#oci) or [tar archives](#tar) provided here, in
conjunction with your preferred container tool.

More information about the container runtime is available in the
[steam-runtime-tools documentation][], particularly the
[guide for game developers][].

[non-Steam game]: https://help.steampowered.com/en/faqs/view/4B8B-9697-2338-40EC
[devkit tool]: https://partner.steamgames.com/doc/steamdeck/loadgames
[steam-runtime-tools documentation]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/tree/master/docs
[guide for game developers]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/slr-for-game-developers.md

## Differences between the Steam container runtime and these container images

Normally, the Steam container runtime will take graphics drivers from
the host system, because this is the only thing that is guaranteed to
work for all GPUs: in particular, NVIDIA GPUs require user-space drivers
that are upgraded in lockstep with the host system's kernel modules.

In order to use these graphics drivers, the Steam container runtime will
often also need to take supporting libraries such as glibc and libX11
from the host system, so the versions of these libraries that are loaded
into a game will often be newer than the ones provided in the runtime.
The newer the host operating system is, and the older the container
runtime is, the more libraries from the host will need to be used.

When using the Platform container images provided here with a conventional
container tool such as Docker or Podman, none of this is done.
Instead, the software will receive a "pure" sniper Platform environment,
with the graphics drivers and supporting libraries from the Platform image,
even if they are older than the versions on the host system.
This is a very strict lowest-common-denominator runtime environment.
Software that works in the Platform environment should also work with the
full Steam container runtime environment, but some software that makes
assumptions about the host operating system will not necessarily work
in the pure Platform environment.

The graphics drivers in the Platform image are relatively old, and
are unlikely to work well with recent hardware.
To work around this, it might be necessary to force use of
software rendering (`llvmpipe`) by setting the environment variable
`LIBGL_ALWAYS_SOFTWARE=1`.
The full Steam container runtime avoids this problem by using graphics
drivers from the host system.

Ordinary container tools like Docker and Podman will usually not share
the user's real home directory with the container.
This is desirable for continuous integration and similar systems, but
not necessarily desirable for running games.
If shared directories or other shared state are required, please consult
your chosen container tool's documentation.

Similarly, ordinary container tools like Docker and Podman will usually
not share the real IPC sockets for X11, Wayland, D-Bus and similar protocols
with the container.
If sharing these sockets is required, please consult your chosen container
tool's documentation.

There are some other differences in the content of the runtime itself:

* In the Steam container runtime, file permissions are normalized to
    `u=rwX,og=rX` and all files are owned by the current user;
    in the Platform container images published here, permissions and
    ownership are normal
* In the Steam container runtime, nothing is setuid or setgid;
    in the Platform container images published here, permissions are normal
* In the Steam container runtime, locations like `/etc` and `/usr` are
    mounted read-only, and apt and dpkg are excluded to save space;
    in the Platform container images published here, all directories are
    read/write, and apt and dpkg are included, so you can install more
    packages if you need to
* In the Steam container runtime, some normally Essential-flagged packages
    like login, mount and passwd are removed to save space;
    in the Platform container images published here, they are still present
* In the Steam container runtime, `/etc` and `/var` have been optimized
    for space;
    in the Platform container images published here, they are
    relatively complete
* The Platform container images published here add sudo and libcap2-bin,
    making it possible to run them with Toolbx
* The Steam container runtime is merged-/usr,
    but the Platform container images published here are not

## OCI images for Docker and Podman <a name="oci"></a>

For Docker:

    sudo docker pull registry.gitlab.steamos.cloud/steamrt/sniper/platform

or for [Podman](https://podman.io/) or [Toolbx](https://containertoolbx.org/):

    podman pull registry.gitlab.steamos.cloud/steamrt/sniper/platform

See steamrt/sniper/sdk> for more details of how to use these images
with Docker, Podman or Toolbx, substituting `platform` for `sdk` in the
image names.

## Tar archives for non-OCI environments <a name="tar"></a>

If you are not using the official Steam container runtime, then using
Docker or Podman is recommended, but the same content is also available
in a flat tar archive for use with non-OCI tools.

The `com.valvesoftware.SteamRuntime.Platform-amd64,i386-sniper-runtime.tar.gz`
archives available from
<https://repo.steampowered.com/steamrt3/images/>
have the same content as these OCI images.
The `files/` directory in the archive should be renamed to `usr/`
and used as the `/usr` directory of a chroot or container.

The chroot or container will also need the usual compatibility symlinks
`/bin -> usr/bin`, `/lib -> usr/lib`, `/lib64 -> usr/lib64` and
`/sbin -> usr/sbin`.

For some uses of the chroot or container, the template `/etc` and `/var`
from the tar archive's `files/etc/` and `files/var/` will need to be
copied to the chroot or container's `/etc` and `/var`.

## Running the Steam container runtime without Steam <a name="slr-without-steam"></a>

Each recent versioned subdirectory of
<https://repo.steampowered.com/steamrt3/images/>
contains an archive named `SteamLinuxRuntime_sniper.tar.xz`, which
is used to build the official `SteamLinuxRuntime_sniper` releases.
These can also be used without going via Steam.
To do this, download and unpack `SteamLinuxRuntime_sniper.tar.xz`.
It unpacks into a directory named `SteamLinuxRuntime_sniper/` which is
equivalent to what is distributed via Steam: for example,

    .../SteamLinuxRuntime_sniper/run -- xterm

will launch an interactive terminal in the container environment.

See the [steam-runtime-tools documentation][] for more information about
how this environment can be used, particularly in the
[guide for game developers][].

### Older versions

Older versions of `steamrt3` had an archive named
`steam-container-runtime-complete.tar.gz`, which unpacks into several
directories, including one named `depot/`.
The `depot/` directory can be renamed to `SteamLinuxRuntime_sniper/`,
and the other directories are not usually necessary and can be discarded.

## 32-bit code

The `registry.gitlab.steamos.cloud/steamrt/sniper/platform` image is
primarily 64-bit (`x86_64`), but also contains a complete set of 32-bit
runtime libraries, so it can run either 32- or 64-bit games.

## apt packages

All of the packages that are supported for use in Steam games are included
in the Platform image and do not need to be downloaded separately.

However, the Platform image does contain `apt` and `dpkg`, to make it
easier to install debugging tools.

See steamrt/sniper/sdk> for more details of the apt repositories that
were used to build this image.

## Source code

Source code for all the packages that go into the OCI image can be found
in the appropriate subdirectory of
<https://repo.steampowered.com/steamrt3/images/>
(look in the `sources/` directory).
